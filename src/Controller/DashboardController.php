<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route(path: ['fr' => '/dashboard'], name: 'app_dashboard')]
class DashboardController extends AbstractController
{
    #[Route(path: '/', name: '', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function dashboard(): Response {
        return $this->render('base.html.twig');
    }
}
