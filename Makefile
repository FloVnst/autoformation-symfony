SYMFONY_CLI = symfony
SYMFONY_PROJECT_CLI = php bin/console

watch:
	npm run watch

serve:
	$(SYMFONY_CLI) server:start

psalm:
	php vendor/bin/psalm

rector:
	php vendor/bin/rector

cs:
	PHP_CS_FIXER_IGNORE_ENV=1 php tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src/

qa: rector psalm cs

cc:
	$(SYMFONY_PROJECT_CLI) cache:clear
